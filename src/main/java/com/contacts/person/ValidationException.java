package com.contacts.person;

public class ValidationException extends RuntimeException {
    private String description;

    public ValidationException(String msg, String description){
        super(msg);
        this.description = description;
    }

    public String getDescription(){
        return description;
    }
}
